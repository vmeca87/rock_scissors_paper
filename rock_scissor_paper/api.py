import connexion
OPTIONS = ['rock', 'paper', 'scissors']


def match(player_a, player_b):
    """
    returns 1 if player_a wins, -1 if he lose and 0 for tie

    """
    index_player_a, index_player_b = process(player_a, player_b)
    d = (index_player_a - index_player_b) % len(OPTIONS)
    return -1 if d == 2 else d


def process(player_a, player_b):
    """
    Returns a tuple of the integer represtation of the
    player option or a exception if it is no a valid option
    """
    player_options = [player_a, player_b]

    if not all(elem in OPTIONS for elem in player_options):
        msg = 'The options {player_options} are not valid, the valid' +\
              'options are {valid_options}'
        msg = msg.format(player_options=player_options, valid_options=OPTIONS)
        raise Exception(msg)

    index_player_a = OPTIONS.index(player_a)
    index_player_b = OPTIONS.index(player_b)

    return (index_player_a, index_player_b)


def main():
    app = connexion.FlaskApp(__name__, port=9090, specification_dir='swagger/')
    app.add_api('api.yaml')
    from waitress import serve
    serve(app.app, listen='*:8080')
