from rock_scissor_paper import api
import pytest


TEST_DATA = [
    ('rock', 'scissors', 1),
    ('rock', 'paper', -1),
    ('rock', 'rock', 0),
    ('paper', 'scissors', -1),
    ('paper', 'paper', 0),
    ('paper', 'rock', 1),
    ('scissors', 'scissors', 0),
    ('scissors', 'paper', 1),
    ('scissors', 'rock', -1),
]


@pytest.mark.parametrize("player_a,player_b,expected", TEST_DATA)
def test_match_01(player_a, player_b, expected):
    assert expected == api.match(player_a, player_b)


def test_match_02():
    with pytest.raises(Exception) as excinfo:
        api.match('invalid', 'paper')
    assert 'are not valid' in str(excinfo.value)
